import React from "react";
import Header from "./Header";

class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            token: this.props.location.state.token
        };
        console.log(props);
    }

    render() {
        return (
            <Header token={this.state.token}/>
        )
    }
}

export default Home;