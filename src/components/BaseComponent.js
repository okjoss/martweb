import React from "react";

class BaseComponent extends React.Component {
    constructor(props) {
        super(props);
        this.baseApiUrl = "http://localhost:5000/api/v1/";
    }
}

export default BaseComponent;