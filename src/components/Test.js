import React from "react";

class Test extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            logins: []
        }
    }

    componentDidMount() {
        fetch('http://localhost:5000/api/v1/logins')
        .then(res => res.json())
        .then(json => {
            this.setState({
                isLoaded: true,
                logins: json
            })
        })
    }
    
    render() {
        var {isLoaded, logins} = this.state;

        if (!isLoaded) {
            return "<div>Loading....</div>";
        }
        else {
            console.log(this.state.logins);
            return (
                <div>
                    <ul>
                        {logins.map(item => (
                            <li>{item.username}</li>
                        ))}
                    </ul>
                </div>
            );
        }
    }
}

export default Test;