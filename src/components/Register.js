import BaseComponent from "./BaseComponent";
import React from "react"
import { Redirect } from 'react-router-dom';

class Register extends BaseComponent {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            result: []
        };
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState(
            {[event.target.id]: event.target.value}
        );
    }

    handleSubmit = event => {
        event.preventDefault();

        console.log(this.baseApiUrl);
        fetch(this.baseApiUrl + "auth/register", {
			method: 'POST',
			body: JSON.stringify({
				username: this.state.username,
				password: this.state.password
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			}
		}).then(response => {
            return response.json()
        }).then(json => {
            this.setState({
                result:json
            });

            console.log(this.state.result);
        });
    }

    render() {
        if (this.state.result.errorMessage === "") {
            return <Redirect to='/Login' />;
        }
        return (
            <div>
                <div className="container">
                    <div className="row mt-2">
                        <div className="col-sm"></div>
                        <div className="col-sm">
                            <form onSubmit={this.handleSubmit}>
                                <h2 className="text-center">Register</h2>
                                <div className="bg-danger"><p>{this.state.result["errorMessage"]}</p></div>
                                <div className="form-group">
                                    <label>Username</label>
                                    <input type="text" className="form-control" id="username" placeholder="Username" onChange={this.handleChange}/>
                                </div>
                                <div className="form-group">
                                    <label>Password</label>
                                    <input type="password" className="form-control" id="password" placeholder="Password" onChange={this.handleChange}/>
                                </div>
                                <div className="form-group">
                                    <button type="submit" className="btn btn-primary btn-block" disabled={!this.validateForm()}>Submit</button>
                                </div>
                            </form>
                        </div>
                        <div className="col-sm"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register