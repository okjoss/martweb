import BaseComponent from "./BaseComponent";
import React from "react";
import { Redirect } from 'react-router-dom';

class Header extends BaseComponent {
    constructor(props) {
      super(props);

      this.state = {
          token: this.props.token,
          result: []
      };
    }

    handleLogout = event => {
      fetch(this.baseApiUrl, {
        method: 'GET',
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          "token": this.state.token
        }
      }).then(response => {
          return response.json()
      }).then(json => {
          this.setState({
              result:json
          });

          console.log(this.state.result);
      });
    }
    
    render() {
        if (this.state.token === "") {
          return <Redirect to='/Login' />;
        }
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <a className="navbar-brand">OKMart</a>
          
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <a className="nav-link">Home <span className="sr-only">(current)</span></a>
                </li>
              </ul>
    
            </div>
            <div className="bg-danger">{this.state.result.errorMessage}</div><a className="nav-link" onClick={this.handleLogout} href="/">Logout</a>
          </nav>
        )
    }
}

export default Header;